# Kivy Hello World

## Description

Kivy Hello World is an example app you can fork and edit to submit your application to f-droid, or to build and release it using Gitlab CI/CD.

## F-dorid

F-dorid Is a repository for open source software. You can submit your app using the 
[Submitting to F-Droid Quick Start Guide](https://f-droid.org/en/docs/Submitting_to_F-Droid_Quick_Start_Guide/)

Hopefully this example app will have the necessery things needed for submission.


## Files

Other than Kivy files `main.py` and `buildozer.spec` files there are some files that are needed or recommeneded for building.

 * com.gitlab.uak.kivy_hello_world.yml : [build metadata reference](https://f-droid.org/en/docs/Build_Metadata_Reference/) file. should be submitted to https://gitlab.com/fdroid/fdroiddata/
 * metadata/en-US : folder for app metadata (needed for inclusion in f-droid)
 * Pipfile & Pipfile.lock : manages dependencies (not required for f-droid)
